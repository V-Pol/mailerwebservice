var builder = WebApplication.CreateBuilder(args);
builder.Configuration.AddJsonFile("SMTPSettings.json");
var services = builder.Services;
services.Configure<SMTPConfig>(builder.Configuration.GetSection("SMTPConfig"));
services.AddControllers().AddJsonOptions(options =>
{
    options.JsonSerializerOptions.PropertyNamingPolicy = null;
    options.JsonSerializerOptions.WriteIndented = true;
});

services.AddEndpointsApiExplorer();

var connectionString = builder.Configuration.GetConnectionString("Mailer");
services.AddDbContextPool<ApplicationDbContext>(options =>
        options.UseSqlServer(connectionString, sqlOptions => sqlOptions.EnableRetryOnFailure().CommandTimeout(60)));

services
    .AddScoped<IResultRepo, ResultRepo>()
    .AddScoped<IMailRepo, MailRepo>()
    .AddScoped<IRecipientRepo, RecipientRepo>()
    .AddScoped<ISendingRepo, SendingRepo>()
    .AddScoped<EmailService>();

services.AddSwaggerGen(options =>
{
    options.EnableAnnotations();
    options.SwaggerDoc("v1",
        new OpenApiInfo
        {
            Title = "Mailer Web Service",
            Version = "v1",
            Description = "Web-������ ������������ � �������� ����������� �����",
            License = new OpenApiLicense
            {
                Name = "��������� ��������� ���������",
                Url = new Uri("https://v-pol.gitlab.io/representationsite")
            }
        });
    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.Api.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    options.IncludeXmlComments(xmlPath);
});

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
    if (builder.Configuration.GetValue<bool>("RebuildDataBase"))
    {
        using var scope = app.Services.CreateScope();
        var dbContext = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
        SampleDataInitializer.InitializeData(dbContext);
    }
}

app.UseHttpsRedirection();
app.UseSwagger();
app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "MailerWebService v1"); });
app.MapControllers();

app.Run();
