﻿namespace MailerWebService.Models.Entities;

public partial class Mail : BaseEntity
{
    [StringLength(50)]
    public string Subject { get; set; } = null!;

    public string? Body { get; set; }

    [Required]
    public int ResultId { get; set; }

    public string? FailedMessage { get; set; }

    [NotMapped]
    public string[]? Recipients { get; set; }

    [Column(TypeName = "datetime")]
    public DateTime? CreatingDateTime { get; set; }

    [ForeignKey("ResultId")]
    [InverseProperty(nameof(Entities.Result.Mail))]
    public Result Result { get; set; } = null!;

    [JsonIgnore]
    [InverseProperty(nameof(Entities.Sending.Mail))]
    public IEnumerable<Sending> Sendings { get; set; } = new List<Sending>();
}
