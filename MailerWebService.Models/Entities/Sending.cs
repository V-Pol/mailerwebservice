﻿namespace MailerWebService.Models.Entities;

public partial class Sending : BaseEntity
{
    [Required]
    public int MailId { get; set; }

    [Required]
    public int RecipientId { get; set; }

    [ForeignKey("MailId")]
    [InverseProperty(nameof(Entities.Mail.Sendings))]
    public Mail Mail { get; set; } = null!;

    [ForeignKey("RecipientId")]
    [InverseProperty(nameof(Entities.Recipient.Sendings))]
    public Recipient Recipient { get; set; } = null!;
}
