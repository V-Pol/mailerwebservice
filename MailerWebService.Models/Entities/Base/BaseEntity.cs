﻿namespace MailerWebService.Models.Entities
{
    /// <summary>
    /// Base abstract class for entities
    /// </summary>
    public abstract class BaseEntity
    {
        /// <summary>
        /// Identification field
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonIgnore]
        public int Id { get; set; }
    }
}
