﻿namespace MailerWebService.Models.Entities;

[Table("Result")]
public partial class Result : BaseEntity
{
    [StringLength(10)]
    public string? Status { get; set; }

    [JsonIgnore]
    [InverseProperty(nameof(Entities.Mail.Result))]
    public IEnumerable<Mail> Mail { get; set; } = new List<Mail>();
}
