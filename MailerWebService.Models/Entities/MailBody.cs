﻿namespace MailerWebService.Models.Entities;
/// <summary>
/// Mails class for POST request 
/// </summary>
public partial class MailBody
{
    [StringLength(50)]
    public string Subject { get; set; } = null!;

    public string? Body { get; set; }

    public string[]? Recipients { get; set; }
}
