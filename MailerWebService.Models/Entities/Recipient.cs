﻿namespace MailerWebService.Models.Entities;

public partial class Recipient : BaseEntity
{
    [StringLength(50)]
    public string Address { get; set; } = null!;

    [JsonIgnore]
    [InverseProperty(nameof(Entities.Sending.Recipient))]
    public IEnumerable<Sending> Sendings { get; set; } = new List<Sending>();
}
