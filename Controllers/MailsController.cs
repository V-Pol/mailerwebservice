﻿namespace WebReporting.Api.Controllers
{
    /// <summary>
    /// Controller for mail API
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class MailsController
    {
        private readonly IMailRepo _mailRepo;
        private readonly IRecipientRepo _recipientRepo;
        private readonly ISendingRepo _sendingRepo;
        private readonly EmailService _emailService;
        public MailsController(IMailRepo mailRepo, IRecipientRepo recipientRepo,ISendingRepo sendingRepo, EmailService emailService)
        {
            _mailRepo = mailRepo;
            _recipientRepo = recipientRepo;
            _sendingRepo = sendingRepo;
            _emailService = emailService;
        }

        /// <summary>
        /// Gets all records
        /// </summary>
        /// <returns>All records</returns>
        /// <response code="200">Returns all items</response>
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [SwaggerResponse(200, "The execution was successful")]
        [HttpGet]
        public virtual ActionResult<IEnumerable<Mail>> GetAll()
        {
            return new OkObjectResult(_mailRepo.GetAll());
        }

        /// <summary>
        /// Adds a single record
        /// </summary>
        /// <param name="mailBody">A content data of the mail</param>
        /// <returns>Added record</returns>
        /// <response code="201">Add the record</response>
        /// <response code="500">Failed request</response>
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [SwaggerResponse(201, "The execution was successful")]
        [SwaggerResponse(500, "The execution was invalid")]
        [HttpPost]
        public ActionResult<Mail> Add(MailBody mailBody)
        {
            var entity = new Mail {Subject = mailBody.Subject, Body = mailBody.Body, ResultId = 1 };
            entity.CreatingDateTime = DateTime.Now;
            try
            {
                try
                {
                    _emailService.SendEmail(mailBody.Recipients!, mailBody.Subject!, mailBody.Body!);
                }
                catch (Exception ex)
                {
                    entity.ResultId = 2;
                    entity.FailedMessage = ex.Message;
                }

                _mailRepo.Add(entity);

                foreach (var recipientAddress in mailBody.Recipients!)
                {
                    var recipient = _recipientRepo.GetOneByAddress(recipientAddress);
                    if (recipient == null)
                    {
                        recipient = new Recipient { Address = recipientAddress };
                        _recipientRepo.Add(recipient);
                    }
                    
                    _sendingRepo.Add(new Sending { MailId = entity!.Id, RecipientId = recipient.Id });
                }

                return new StatusCodeResult(StatusCodes.Status201Created);
            }
            catch (Exception ex)
            {
                return new StatusCodeResult(StatusCodes.Status500InternalServerError);
            }
        }
    }
}