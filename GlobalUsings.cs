﻿global using MailerWebService.Dal.EfStructures;
global using MailerWebService.Dal.Initialization;
global using MailerWebService.Dal.Repos;
global using MailerWebService.Dal.Repos.Base;
global using MailerWebService.Dal.Repos.Interfaces;

global using MailerWebService.Models.Entities;

global using MailerWebService.Services;

global using Microsoft.AspNetCore.Mvc;
global using Microsoft.EntityFrameworkCore;
global using Microsoft.OpenApi.Models;

global using Swashbuckle.AspNetCore.Annotations;

global using System.Reflection;