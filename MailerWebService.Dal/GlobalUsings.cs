﻿global using MailerWebService.Dal.EfStructures;
global using MailerWebService.Dal.Repos.Base;
global using MailerWebService.Dal.Repos.Interfaces;

global using MailerWebService.Models.Entities;

global using Microsoft.EntityFrameworkCore;
global using Microsoft.EntityFrameworkCore.Design;
global using Microsoft.EntityFrameworkCore.Infrastructure;
global using Microsoft.EntityFrameworkCore.Migrations;
global using Microsoft.EntityFrameworkCore.Storage;

global using System.Data;