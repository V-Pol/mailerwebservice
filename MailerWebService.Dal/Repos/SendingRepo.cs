﻿namespace MailerWebService.Dal.Repos
{
    public class SendingRepo : BaseRepo<Sending>, ISendingRepo
    {
        public SendingRepo(ApplicationDbContext context) : base(context)
        {
        }

        internal SendingRepo(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public override IEnumerable<Sending> GetAll()
        => Table
            .Include(x => x.Mail)
            .Include(x => x.Recipient)
            .OrderBy(a => a.Id);
    }
}