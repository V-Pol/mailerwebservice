﻿namespace MailerWebService.Dal.Repos.Base
{
    /// <summary>
    /// Base class for Repositories
    /// </summary>
    public abstract class BaseRepo<T> : IRepo<T> where T : BaseEntity, new()
    {
        private readonly bool _disposeContext;
        public DbSet<T> Table { get; }
        public ApplicationDbContext Context { get; }

        protected BaseRepo(ApplicationDbContext context)
        {
            Context = context;
            Table = Context.Set<T>();
            _disposeContext = false;
        }

        protected BaseRepo(DbContextOptions<ApplicationDbContext> options) : this(new ApplicationDbContext(options))
        {
            _disposeContext = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private bool _isDisposed;

        protected virtual void Dispose(bool disposing)
        {
            if (_isDisposed)
            {
                return;
            }

            if (disposing)
            {
                if (_disposeContext)
                {
                    Context.Dispose();
                }
            }

            _isDisposed = true;
        }

        ~BaseRepo()
        {
            Dispose(false);
        }
        /// <summary>
        /// Search for a record by id
        /// </summary>
        /// <param name="Id">ID of the object</param>
        /// <returns></returns>
        public virtual T? Find(int? id) => Table.Find(id);

        /// <summary>
        /// Search for a message by id in async style
        /// </summary>
        /// <param name="Id">ID of the Mail object</param>
        /// <returns></returns>
        public virtual async Task<T?> FindAsync(int? id) => await Table.FindAsync(id);

        /// <summary>
        /// Get all records
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<T> GetAll() => Table;

        /// <summary>
        /// Adds the entity to the database
        /// </summary>
        /// <param name="entity">The entity</param>
        /// <returns>Return bool status of the adding</returns>
        public virtual bool Add(T entity)
        {
            if (Table.Add(entity) != null)
            {
                Context.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}