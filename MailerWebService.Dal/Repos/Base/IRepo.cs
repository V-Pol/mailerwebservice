﻿namespace MailerWebService.Dal.Repos.Base
{
    public interface IRepo<T>: IDisposable 
    {
        bool Add(T entity);
        T? Find(int? id);
        Task<T?> FindAsync(int? id);
        IEnumerable<T> GetAll();
    }
}