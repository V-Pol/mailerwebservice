﻿namespace MailerWebService.Dal.Repos
{
    public class ResultRepo : BaseRepo<Result>, IResultRepo
    {
        public ResultRepo(ApplicationDbContext context) : base(context)
        {
        }

        internal ResultRepo(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
    }
}