﻿namespace MailerWebService.Dal.Repos.Interfaces
{
    public interface IRecipientRepo : IRepo<Recipient>
    {
        Recipient? GetOneByAddress(string address);
    }
}
