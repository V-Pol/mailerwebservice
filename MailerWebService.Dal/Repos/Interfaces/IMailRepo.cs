﻿namespace MailerWebService.Dal.Repos.Interfaces
{
    public interface IMailRepo : IRepo<Mail>
    {
    }
}
