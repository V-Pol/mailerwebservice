﻿namespace MailerWebService.Dal.Repos.Interfaces
{
    public interface ISendingRepo : IRepo<Sending>
    {
    }
}
