﻿using MailerWebService.Dal.Repos.Interfaces;

namespace MailerWebService.Dal.Repos
{
    public class RecipientRepo : BaseRepo<Recipient>, IRecipientRepo
    {
        public RecipientRepo(ApplicationDbContext context) : base(context)
        {
        }

        internal RecipientRepo(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        /// <summary>
        /// Gets one recipient by email address
        /// </summary>
        /// <param name="address">Email address</param>
        /// <returns>One recipient or null</returns>
        public Recipient? GetOneByAddress(string address)
        {
            return Table.FirstOrDefault(r => r.Address == address);
        }
    }
}