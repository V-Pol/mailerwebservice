﻿namespace MailerWebService.Dal.Repos
{
    /// <summary>
    /// A class for managing the context of a mail in DB
    /// </summary>
    public class MailRepo : BaseRepo<Mail>, IMailRepo
    {
        ApplicationDbContext _context;
        public MailRepo(ApplicationDbContext context) : base(context)
        {
            _context = context;
        }

        /// <summary>
        /// Override method to get all mails
        /// </summary>
        /// <returns>IQueryable&lt;Mail&gt;</returns>
        public override IEnumerable<Mail> GetAll()
        {
            var result = Table.Select(t => new Mail
            {
                Id = t.Id,
                Subject = t.Subject,
                Body= t.Body,
                Recipients = _context.Recipients!
                    .Select(r => r.Sendings
                                .Where(s => s.MailId == t.Id)
                                .Select(s => s.Recipient.Address)
                                .First())
                    .Where(x => x != null)
                    .ToArray(),
                CreatingDateTime = t.CreatingDateTime,
                ResultId = t.ResultId,
                Result = t.Result,
                FailedMessage= t.FailedMessage,
            });
            return result;
        }
    }
}