namespace MailerWebService.Dal.Initialization
{
    /// <summary>
    /// A class to the initialization test DB
    /// </summary>
    public static class SampleDataInitializer
    {
        internal static void ClearData(ApplicationDbContext context)
        {
            var entities = new[]
            {
                typeof(Result).FullName,
                typeof(Recipient).FullName,
                typeof(Mail).FullName,
                typeof(Sending).FullName,
            };
            foreach (var entityName in entities)
            {
                var entity = context.Model.FindEntityType(entityName!);
                var tableName = entity!.GetTableName();
                var schemaName = entity!.GetSchema();
                context.Database.ExecuteSqlRaw($"DELETE FROM {schemaName}.{tableName}");
                context.Database.ExecuteSqlRaw($"DBCC CHECKIDENT (\"{schemaName}.{tableName}\", RESEED, 1);");
            }
        }

        internal static void SeedData(ApplicationDbContext context)
        {
            try
            {
                ProcessInsert(context, context.Results!, SampleData.Results);
                ProcessInsert(context, context.Mails!, SampleData.Mails);
                ProcessInsert(context, context.Recipients!, SampleData.Recipients);
                ProcessInsert(context, context.Sendings!, SampleData.Sendings);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
            static void ProcessInsert<TEntity>(
                ApplicationDbContext context, DbSet<TEntity> table, List<TEntity> records) where TEntity : BaseEntity
            {
                if (table.Any())
                {
                    return;
                }

                IExecutionStrategy strategy = context.Database.CreateExecutionStrategy();
                strategy.Execute(() =>
                {
                    using var transaction = context.Database.BeginTransaction();
                    try
                    {
                        var metaData = context.Model.FindEntityType(typeof(TEntity)!.FullName!);
                        context.Database.ExecuteSqlRaw(
                            $"SET IDENTITY_INSERT {metaData!.GetSchema()}.{metaData!.GetTableName()} ON");
                        table.AddRange(records);
                        context.SaveChanges();
                        context.Database.ExecuteSqlRaw(
                            $"SET IDENTITY_INSERT {metaData!.GetSchema()}.{metaData!.GetTableName()} OFF");
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                    }
                });
            }
        }


        internal static void DropAndCreateDatabase(ApplicationDbContext context)
        {
            context.Database.EnsureDeleted();
            context.Database.Migrate();
        }

        /// <summary>
        /// Initialization test data method
        /// Include drop DB, create DB and seed data
        /// </summary>
        public static void InitializeData(ApplicationDbContext context)
        {
            DropAndCreateDatabase(context);
            SeedData(context);
        }

        /// <summary>
        /// Clear and reseed data method
        /// Include clear data from DB and seed data to DB
        /// </summary>
        public static void ClearAndReseedDatabase(ApplicationDbContext context)
        {
            ClearData(context);
            SeedData(context);
        }
    }
}