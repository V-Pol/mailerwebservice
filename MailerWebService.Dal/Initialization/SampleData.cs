namespace MailerWebService.Dal.Initialization
{
    /// <summary>
    /// A class with sample data to the initialization test DB
    /// </summary>
    public static class SampleData
    {
        public static List<Result> Results => new()
        {
            new() {Id = 1, Status =  "OK"},
            new() {Id = 2, Status =  "Failed"}
        };

        public static List<Mail> Mails => new()
        {
            new() {Id = 1, Subject = "����", Body = "�������� ������", ResultId = 1, CreatingDateTime = DateTime.Now},
            new() {Id = 2, Subject = "����2", Body = "�������� ������ #2", ResultId = 2, FailedMessage = "Unknown Error", CreatingDateTime = DateTime.Now}
        };

        public static List<Recipient> Recipients => new()
        {
            new() {Id = 1, Address = "test1@mail.ru"},
            new() {Id = 2, Address = "test2@mail.ru"},
            new() {Id = 3, Address = "test3@mail.ru"},
            new() {Id = 4, Address = "test4@mail.ru"}
        };

        public static List<Sending> Sendings => new()
        {
            new() {Id = 1, MailId = 1, RecipientId = 1},
            new() {Id = 2, MailId = 1, RecipientId = 2},
            new() {Id = 3, MailId = 2, RecipientId = 1},
            new() {Id = 4, MailId = 2, RecipientId = 3}
        };
    }
}