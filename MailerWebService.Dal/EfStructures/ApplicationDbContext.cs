﻿namespace MailerWebService.Dal.EfStructures;

public partial class ApplicationDbContext : DbContext
{
    /// <summary>
    /// Application DB context for the creating DB
    /// </summary>
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
    : base(options)
    {
    }
    public DbSet<Mail>? Mails { get; set; }

    public DbSet<Recipient>? Recipients { get; set; }

    public DbSet<Result>? Results { get; set; }

    public DbSet<Sending>? Sendings { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Mail>(entity =>
        {
            entity.HasOne(d => d.Result).WithMany(p => p!.Mail)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Mails_Result");
        });

        modelBuilder.Entity<Result>(entity =>
        {
            entity.Property(e => e.Status).IsFixedLength();
        });

        modelBuilder.Entity<Sending>(entity =>
        {
            entity.HasOne(d => d.Mail).WithMany(p => p!.Sendings)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Sendings_Mails1");

            entity.HasOne(d => d.Recipient).WithMany(p => p!.Sendings)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Sendings_Recipients1");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
