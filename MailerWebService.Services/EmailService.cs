﻿using MimeKit;
using MailKit.Net.Smtp;
using static Org.BouncyCastle.Math.EC.ECCurve;
using MailerWebService.Models.Entities;
using static MailerWebService.Services.SMTPConfig;
using Microsoft.Extensions.Options;

namespace MailerWebService.Services
{
    /// <summary>
    /// Email sending service
    /// </summary>
    public class EmailService
    {
        private readonly SMTPConfig _config;
        public EmailService(IOptions<SMTPConfig> smtpConfig)
        {
            _config = smtpConfig.Value;
        }

        /// <summary>
        /// Publishing message to mails 
        /// </summary>
        /// <param name="emails">Array of recipients</param>
        /// <param name="subject">Subject message</param>
        /// <param name="emails">Body of message</param>
        /// <returns></returns>
        public void SendEmail(string[] emails, string subject, string body)
        {
            List<MimeMessage> messages = new List<MimeMessage>();

            foreach (var recipient in emails)
            {
                var emailMessage = new MimeMessage();
                emailMessage.From.Add(new MailboxAddress(_config.Sender, _config.Email));
                emailMessage.To.Add(new MailboxAddress("", recipient));
                emailMessage.Subject = subject;
                emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
                {
                    Text = body
                };
                messages.Add(emailMessage);
            }

            foreach (var message in messages)
            {
                SendBySMTPClient(message);
            }
        }

        private void SendBySMTPClient(MimeMessage message)
        {
            using (var client = new SmtpClient())
            {
                client.Connect(_config.Host, _config.Port, _config.UseSsl);
                client.Authenticate(_config.Email, _config.Password);
                client.Send(message);
                client.Disconnect(true);
            }
        }


    }
}
