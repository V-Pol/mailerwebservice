﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailerWebService.Services
{
    /// <summary>
    /// SMTP configuration
    /// </summary>
    public class SMTPConfig
    {
        /// <summary>
        /// Sender name
        /// </summary>
        public string Sender { get; set; }

        /// <summary>
        /// Sender email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Adress of the SMTP host 
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// Port of the SMTP host
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Use SSL on the SMTP host
        /// </summary>
        public bool UseSsl { get; set; }
    }
}