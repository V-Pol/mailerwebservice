﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240117101230_Initial')
BEGIN
    CREATE TABLE [Recipients] (
        [Id] int NOT NULL,
        [Address] nvarchar(50) NOT NULL,
        CONSTRAINT [PK_Recipients] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240117101230_Initial')
BEGIN
    CREATE TABLE [Result] (
        [Id] int NOT NULL,
        [Status] nchar(10) NULL,
        CONSTRAINT [PK_Result] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240117101230_Initial')
BEGIN
    CREATE TABLE [Mails] (
        [Id] int NOT NULL,
        [Subject] nvarchar(50) NOT NULL,
        [Body] nvarchar(max) NULL,
        [ResultId] int NOT NULL,
        [FailedMessage] nvarchar(max) NULL,
        [TimeStamp] rowversion NOT NULL,
        CONSTRAINT [PK_Mails] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Mails_Result] FOREIGN KEY ([ResultId]) REFERENCES [Result] ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240117101230_Initial')
BEGIN
    CREATE TABLE [Sendings] (
        [Id] int NOT NULL,
        [MailId] int NOT NULL,
        [RecipientId] int NOT NULL,
        CONSTRAINT [PK_Sendings] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Sendings_Mails1] FOREIGN KEY ([MailId]) REFERENCES [Mails] ([Id]),
        CONSTRAINT [FK_Sendings_Recipients1] FOREIGN KEY ([RecipientId]) REFERENCES [Recipients] ([Id])
    );
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240117101230_Initial')
BEGIN
    CREATE INDEX [IX_Mails_ResultId] ON [Mails] ([ResultId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240117101230_Initial')
BEGIN
    CREATE INDEX [IX_Sendings_MailId] ON [Sendings] ([MailId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240117101230_Initial')
BEGIN
    CREATE INDEX [IX_Sendings_RecipientId] ON [Sendings] ([RecipientId]);
END;
GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20240117101230_Initial')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20240117101230_Initial', N'7.0.15');
END;
GO

COMMIT;
GO

